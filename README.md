# CaseStudy for Realizing a UI Fragment Repository using AASX Packages

  * *.aasx Files created using the AASX Package Explorer v2020-11-16
  * used UIMDF catalogue: https://agtele.eats.et.tu-dresden.de/uimdf/?version=0.0.3&lang=en
  